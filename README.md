## TVPR Subset
This repository contains annotation files in the PASCAL VOC format for a subset of the [TVPR](http://vrai.dii.univpm.it/re-id-dataset) dataset. Additional the filenames for the training set are saved in *training.txt* and for the validation set in the *validation.txt*.

The file name consists of the video ID and the corresponding frame number. E.g. for the video with the id g001 and the frame 62 the filename is **g001_frame_00062.jpg**